Godot = Play.extend("Godot");

Godot.init = function () {
	this.audio = love.audio.newSource("audio/godot.wav");
	this.audio.setLooping(true);
	this.audio.setVolume(1);

	this.actors = {
		lilman : Actor.new(140,125),
		man : Actor.new(100,120)
	}
	this.assetsBackground = Group.new();
	this.assetsBackground.add(
		Asset.new(120,40,"images/tree.png")
	)

	this.actors.man.setImage("images/man.png");
	this.actors.man.addAnimation("shoulders",2,2);
	this.actors.man.addAnimation("down",1,1);
	this.actors.man.choseText = false;
	this.actors.man.choseState = false;
	this.actors.man.anim = "idle";
	this.actors.man.states = [
	"\"Screw this, let's go!\"",
	"\"But what if he's here in 5 minutes?\"",
	"\"Hey.. now that you mention it.\""
	];

	this.actors.man.stateEffects = [];
	this.actors.man.stateEffects[0] = "shoulders";

	// print(this.actors.cowboy.anim);


	// "This is the longest sentence I've ever made!",

	// this.actors.bandit = Actor.new(210,120);
	this.actors.lilman.setImage("images/lilman.png");
	this.actors.lilman.addAnimation("left",2,2,5);
	this.actors.lilman.states = [
	"\"Ever noticed there is a tree standing there?\"",
	"\"Screw this, let's go!\"",
	"\"Dude, we've been waiting for 10 years. Shall we go now?\"",
	"\"Okay okay.. let's wait a few more minutes.\""

	];

	this.actors.lilman.stateEffects = [];
	this.actors.lilman.stateEffects[0] = "left";

	this.actions = [
		{
			actors : {
				lilman : {
					text : "\"Dude, we've been waiting for 10 years. Shall we go now?\"",
					state : "idle"
				},
				man : {
					text : null,
					state : "idle"
				}
			},
			time : 7
		},
		{
			actors : {
				man : {
					text : "\"But what if he's here in 5 minutes?\"",
					state : "idle"
				},
				lilman : {
					text : null,
					state : "idle"
				}
			},
			time : 7
		},
		{
			actors : {
				lilman : {
					text : "\"Okay okay.. let's wait a few more minutes.\"",
					state : "idle"
				},
				man : {
					text : null,
					state : "idle"
				}
				
			},
			time : 7
		},
		{
			actors : {
				lilman : {
					text : null,
					state : "idle"
				},
				man : {
					text : null,
					state : "idle"
				}
			},
			time : 7
		},
		{
			actors : {
				lilman : {
					state : "idle",
					text : "\"Ever noticed there is a tree standing there?\""
				},
				man : {
					text : null,
					state : "idle"
				}
			},
			time : 7
		},
		{
			actors : {
				man : {
					state : "idle",
					text : "\"Hey.. now that you mention it.\""
				},
				lilman : {
					text : null,
					state : "idle"
				}
			},

			time : 7
		},
		{
			actors : {
				man : {
					state : "idle",
					text : null
				},
				lilman : {
					text : null,
					state : "idle"
				}
			},
			time : 7
		},
		{
			actors : {
				man : {
					state : "idle",
					text : "\"Screw this, let's go!\""
				},
				lilman : {
					text : "\"Screw this, let's go!\"",
					state : "idle"
				}
			},
			time : 9
		}
	]


	Godot.super.init(this);


	this.script.draw = function () {
		love.graphics.draw(this.img,this.x,this.y);
		love.graphics.setFont(Game.fonts.title);
		love.graphics.setColor(80,80,80);
		love.graphics.print("SCRIPT",this.x + 110,this.y + 16);
		love.graphics.setFont(Game.fonts.script);
		love.graphics.printf("lil' Man: Dude, we've been waiting for 10 years. Shall we go now?",112,this.y + 40,this.limit,"left");
		love.graphics.line(112,this.y+53,222,this.y+53);
		love.graphics.printf("Man: But what if he's here in 5 minutes?",112,this.y+57,this.limit,"left");
		love.graphics.line(112,this.y+64,222,this.y+64);
		love.graphics.printf("lil' Man: Okay okay.. let's wait a few more minutes.",112,this.y+67,this.limit,"left");
		love.graphics.line(112,this.y+80,222,this.y+80);
		love.graphics.printf("(silence)",112,this.y+83,this.limit,"left");
		love.graphics.line(112,this.y+90,222,this.y+90);
		love.graphics.printf("lil' Man: Ever noticed there is a tree standing there?",112,this.y+93,this.limit,"left");
		love.graphics.line(112,this.y+107,222,this.y+107);
		love.graphics.printf("Man: Hey.. now that you mention it.",112,this.y+110,this.limit,"left");
		love.graphics.line(112,this.y+117,222,this.y+117);
		love.graphics.printf("(silence)",112,this.y+120,this.limit,"left");
		love.graphics.line(112,this.y+127,222,this.y+127);
		love.graphics.printf("Both: Screw this, let's go!",112,this.y+130,this.limit,"left");
	}
}
