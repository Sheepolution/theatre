Cowboys = Play.extend("Cowboys");

Cowboys.init = function () {
	this.audio = love.audio.newSource("audio/godot.wav");
	this.audio.setLooping(true);
	this.actors = {
		cowboy : Actor.new(80,120),
		bandit : Actor.new(210,120)
	}

	this.actors.cowboy.setImage("images/cowboy.png",29,46);
	this.actors.cowboy.width = 20;
	this.actors.cowboy.height = 47;
	this.actors.cowboy.addAnimation("talk",1,2,5);
	this.actors.cowboy.addAnimation("blow",4,5,3,"once");
	this.actors.cowboy.addAnimation("pull",3,3,1,"loop");
	this.actors.cowboy.choseText = false;
	this.actors.cowboy.choseState = false;
	this.actors.cowboy.anim = "idle";
	this.actors.cowboy.states = [
	"*Blow away gunsmoke*",
	"*Pull the gun*",
	"\"I can't let you do that, Bandit!\"",
	"\"Crime can go suck it!\"",
	"\"You crimed your last crime!\""
	];

	this.actors.cowboy.stateEffects = [];
	this.actors.cowboy.stateEffects[0] = "blow";
	this.actors.cowboy.stateEffects[1] = "pull";

	// print(this.actors.cowboy.anim);


	// "This is the longest sentence I've ever made!",

	// this.actors.bandit = Actor.new(210,120);
	this.actors.bandit.setImage("images/bandit.png",55,52);
	this.actors.bandit.addAnimation("pull",3,3,5);
	this.actors.bandit.addAnimation("dying",4,7,1.2,"once");
	this.actors.bandit.width = 25;
	this.actors.bandit.height = 50;
	this.actors.bandit.offset.x = -34;
	this.actors.bandit.states = [
	"*Dying*",
	"*Pull the gun*",
	"\"No.. this can't be\"",
	"\"Sorry Sheriff, but my next crime is already scheduled!\""];

	this.actors.bandit.stateEffects = [];
	this.actors.bandit.stateEffects[0] = "dying";
	this.actors.bandit.stateEffects[1] = "pull";

	this.actions = [
		{
			actors : {
				cowboy : {
					text : "\"You crimed your last crime!\""
				}
			},
			time : 5
		},
		{
			actors : {
				bandit : {
					text : "\"Sorry Sheriff, but my next crime is already scheduled!\""
				}
			},
			time : 4
		},
		{
			actors : {
				cowboy : {
					text : "\"I can't let you do that, Bandit!\""
				}
			},
			time : 4
		},
		{
			actors : {
				cowboy : {
					state : "pull"
				},
				bandit : {
					state : "pull"
				}
			},
			time : 6
		},
		{
			actors : {
				bandit : {
					state : "dying",
					text : "\"No.. this can't be\""
				}
			},
			time : 5
		},
		{
			actors : {
				cowboy : {
					state : "blow",
					text : "\"Crime can go suck it!\""
				}
			},
			time : 5
		}
	]


	Cowboys.super.init(this);
}
