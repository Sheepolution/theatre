Actor = Button.extend("Actor");

Actor.init = function (x,y,c) {
	Actor.super.init(this,x,y,true,c);
	this.oldState;
	this.states = [];
	this.textObj = {}
	this.textObj.x = -23;
	this.textObj.text = null;
	// this.setFunction(this,"showStates");
	this.showingStates = false;
	this.selectedText = -1;
	this.selectedState = -1;
}

Actor.showStates = function () {
	// if (Play.whoShowStates == null) {
	// 	Play.setStateShower(this);
	// }
}

Actor.update = function () {
	if (Play.choosingState) {
		if (this.clickable) {
			if (this.mouseOver()) {
				if (this.onRelease) {
					if (!Play.clickedExit) {
						if (Input.mouseReleased("l")) {
							if (!Play.clickedSomething) {
								this.obj[this.func].apply(this.obj,this.args);
								Play.clickedSomething = true;
							}
						}
					}
				}
				else if (this.obj) {
					if (!Play.clickedExit) {
						if (Input.mousePressed("l")) {
							if (!Play.clickedSomething) {
								this.obj[this.func].apply(this.obj,this.args);
								Play.clickedSomething = true;
							}
						}
					}
				}
			}
		}
	}
	else {
		Button.super.update(this);
		if (this.textObj.text) {
			// this.textObj.x -= 18 * dt;
			if (this.textObj.x < -this.textObj.text.length*5.1) {
				this.textObj.x = 25;
			}
		}
	}
	
}

Actor.draw = function () {
	Actor.super.draw(this);
	

}
Actor.drawText = function () {
	if (!Play.choosingState) {
		if (this.textObj.text) {
			love.graphics.setColor(0,0,0);
			love.graphics.setColor(255,255,255);
			love.graphics.setFont(Game.fonts.script)
			love.graphics.printf(this.textObj.text,this.middleX(),this.y-18,120,"center");
		}
	}
}