/*
classist

Copyright (c) 2014, Sheepolution

This module is free software; you can redistribute it and/or modify it under
the terms of the MIT license. See LICENSE for details.

Inspired by 'classic' from rxi.
https://github.com/rxi/classic
*/


Object = {};

//AFAIK there is no way to know the name of a variable in JS, so we'll have to do it with this.
Object.names = ["Object"];


Object.extend = function (name) {
	//Extend a new class with this class


	//If no name is given, make it ""
	if (typeof(name) != "string") {
    	name = "";
    }

	var temp = {};

	//The properties of this class
	var supr = {};

	//Clone all properties	
	for(var key in this) {
   		temp[key] = this.__clone(this[key]);

   		//Add modified functions to supr
   		supr[key] = this.__clone(this[key],true);
    }

    //Make this.super refer to the extended class
    temp.super = supr;

    //Add the name
    temp.names.push(name);

    return temp;
}


Object.new = function () {
	//Creates a new instance of the class

	var self = this.__clone(this);

	//Remove self.clone
	self.__clone = null;

	//Initiate
	self.init.apply(self,arguments);

	return self;
}


Object.implement = function (obj) {
	//Adds all functions of obj to the class

	// if(typeof(obj) != "object") {
	// 	return obj;
	// }

	for(var key in obj) {
		if (this[key] == null) {
			this[key] = this.__clone(obj[key]);
		}
	}
}


Object.is = function (obj) {
	//Returns if the class is from type obj

	for (var i = 0; i < this.names.length; i++) {
		if (this.names[i] == obj.name()) {
			return true;
		}
	}
	return false;
}


Object.name = function () {
	//Gets the name of the class

	if (this.names[this.names.length-1].length == 0) {
		throw("Add a type name! class:extend([TYPE])");
	}
	return this.names[this.names.length-1];
}


Object.__clone = function(obj,supr) {

	//Check if this is for the super class
	if (supr) {
		//Check if it's a function
		if (typeof(obj) == "function") {

			//Make obj into a string
			var strObj = obj.toString();

			//Split parameters from the body
			var parameters = strObj.split("{")[0];

			//Modify the body string
			var body = strObj.substring(parameters.length+1,strObj.length-1).split("this").join("_this_");
			
			//Modify the parameters string
			parameters = parameters.split("(")[1].split(")")[0].replace(/\s+/g,"");

			//Make obj a new function with _this_ added as parameter
			if (parameters.length == 0) {
				obj = new Function("_this_",body);
			}
			else {
				obj = new Function("_this_,"+parameters,body);
			}
			return obj;
		}
	}

	//Check if it's an object. If not then just return it
	if(obj == null || typeof(obj) != "object") {
		return obj;
	}

	//Get an object constructor: {}
	var temp = obj.constructor();

	//If the property has properties themselves, clone them as well.
	for(var key in obj) {
		if (love.keyboard.isDown("s")) {
			print(key);
		}
		temp[key] = this.__clone(obj[key],supr);
	}

	return temp;
}