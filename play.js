Play = Object.extend("Play");

Play.whoShowStates;
Play.clickedExit;
Play.clickedSomething;
Play.choosingState;
Play.gameStarted = false;

Play.init = function () {
	Play.gameStarted = false;
	Play.choosingState = false;
	Play.clickedSomething = false;
	Play.clickedExit = false;
	Play.whoShowStates = null;
	this.imgStageBack = love.graphics.newImage("images/stage_back.png");
	this.imgStageFront = love.graphics.newImage("images/stage_front.png");
	// this.actors = Group.new();
	this.fadeout = 1;
	Tween.to(this,1,{fadeout:0}).delay(0.4);

	this.script = Script.new();

	this.number = 0;

	this.errors = 0;
	this.corrects = 0;

	this.bad = Entity.new();
	this.bad.setImage("images/bad.png");
	this.bad.alpha = 0;
	this.bad.middleX((960/2)/3);
	this.bad.middleY((634/2)/3);

	this.good = Entity.new();
	this.good.setImage("images/good.png");
	this.good.alpha = 0;
	this.good.middleX((960/2)/3);
	this.good.middleY((634/2)/3);

	this.curtainsLeft = Entity.new(0,-7);
	this.curtainsLeft.setImage("images/curtains.png");


	this.curtainsRight = Entity.new(150,-7);
	this.curtainsRight.setImage("images/curtains.png");
	this.curtainsRight.scale.x = -1;

	this.openingCurtains = false;


	this.time = 1;
	this.timeSpeed = this.actions[0]["time"];
	this.timerStarted = false;

	for (var key in this.actors) {
		this.actors[key].setFunction(this,"setStateShower",[this.actors[key]]);
	}

	Play.choosingState = true;

	this.buttons = null;
	Play.clickedExit = false;
}

Play.update = function () {
	// this.actors.update();
	Play.clickedSomething = false;
	this.bad.alpha = Math.max(0, this.bad.alpha - dt*2);
	this.good.alpha = Math.max(0, this.good.alpha - dt*2);

	this.bad.scale.x += dt/2;
	this.bad.scale.y += dt/2;
	
	this.good.scale.x += dt/2;
	this.good.scale.y += dt/2;
	if (Play.gameStarted) {
		if (Play.whoShowStates && !this.script.showing) {
			this.buttons.update();
		}


		for (var key in this.actors) {
			this.actors[key].update();
		}

		Play.clickedExit = false;
		this.script.update();

		if (this.timerStarted) {
			this.time += dt / this.timeSpeed;
		}
		else {
			if (!this.script.showing) {
				this.timerStarted = true;
			}
		}

		if (this.time > 2) {
			Play.choosingState = !Play.choosingState;
			Play.whoShowStates = null;
			if (Play.choosingState) {
				var succes = true;
				for (var key in this.actions[this.number]["actors"]) {
					for (var action in this.actions[this.number]["actors"][key]) {
						if (action == "text") {
							if (this.actors[key].textObj.text != this.actions[this.number]["actors"][key]["text"]) {
								succes = false;
							}
						}
						else {
							this.actors[key].oldState = this.actions[this.number]["actors"][key]["state"];
							if (this.actors[key].anim != this.actions[this.number]["actors"][key]["state"]) {
								succes = false;
							}
						}
					}
					// if (this.actors[key].state == this.actions[this.number]["actors"][key]) {
					// 	print("SUCCESSS!!!");
					// }
					// else {
					// 	print("FAAAALL");
					// }
				}
				this.nextStep(succes);
				this.number++;
				if (this.number == this.actions.length) {
					Play.gameStarted = false;
					this.closeCurtains();
					return;
				}
				this.timeSpeed = this.actions[this.number]["time"];
			}
			else {
				if (this.number == this.actions.length -1) {
					this.timeSpeed = 5;
				}
				else {
					this.timeSpeed = 3;
				}
			}
			this.time = 1;
		}
		if (Input.mousePressed("l")) {
			if (!Play.clickedSomething) {
				Play.whoShowStates = null; 
			}
		}
		

	}
	else {
		if (Input.mousePressed("l")) {
			if (!this.openingCurtains) {
				this.startGame();
				this.script.show();
			}
		}
	}
}

Play.draw = function () {
	love.graphics.setColor(255,255,255,255);
	love.graphics.draw(this.imgStageBack);
	if (this.assetsBackground) {
		this.assetsBackground.draw();
	}
	for (var key in this.actors) {
		this.actors[key].draw();
		var s = this.actors[key];
		// love.graphics.rectangle("line",s.x,s.y,s.width,s.height);
	}

	if (this.assetsForeground) {
		this.assetsForeground.draw();
	}

	this.curtainsLeft.draw();
	this.curtainsRight.draw();

	love.graphics.draw(this.imgStageFront);

	for (var key in this.actors) {
		this.actors[key].drawText();
		// love.graphics.rectangle("line",s.x,s.y,s.width,s.height);
	}

	if (Play.whoShowStates) {
		for (var i = 0; i < this.buttons.members.length; i++) {
		// print(Play.whoShowStates.states.length);
			if (i == this.buttons.members.length-1) {
				if (this.buttons.members[i].mouseOver() && !this.script.showing) {
					love.graphics.setColor(100,100,100);
				}
				else {
					love.graphics.setColor(30,50,0);
				}
				love.graphics.rectangle("fill",67,30+18*i,180,15);
				
				love.graphics.setFont(Game.fonts.script);
				love.graphics.setColor(255,255,255);

				love.graphics.printf("- CLOSE -",160,35+18*i,1000,"center");
			}
			else {
				if ((Play.whoShowStates.states[i].substring(0,1) == "*" && !Play.whoShowStates.choseState) || (Play.whoShowStates.states[i].substring(0,1) == "\"" && !Play.whoShowStates.choseText)) {
					if (this.buttons.members[i].mouseOver() && !this.script.showing) {
						love.graphics.setColor(100,100,100);
					}
					else {
						if (Play.whoShowStates.states[i].substring(0,1) == "*") {
						
							love.graphics.setColor(205,205,255);
						}
						else {
							love.graphics.setColor(255,255,150);
						}
					}
				
					love.graphics.rectangle("fill",67,30+18*i,180,15);
					if (Play.whoShowStates.selectedState == i || Play.whoShowStates.selectedText == i) {
						love.graphics.setLineWidth(2);
						love.graphics.setColor(255,255,255);
						love.graphics.rectangle("line",67,30+18*i,180,15)
						love.graphics.setLineWidth(0.5);
						love.graphics.setColor(0,0,0);
						love.graphics.rectangle("line",67,30+18*i,180,15)

					}
					// print("Hey, zou jij even willen luistern naar mij? GODVERDOMME!");
					love.graphics.setFont(Game.fonts.script);
					love.graphics.setColor(0,0,0);
					love.graphics.printf(Play.whoShowStates.states[i],160,35+18*i,1000,"center");
				}
			}
		}
	}

	// this.cowboy.draw();
	// this.bandit.draw();





	if (this.items) {
		this.items.draw();
	}

	this.script.draw();

	this.good.draw();
	this.bad.draw();
	love.graphics.push();
	love.graphics.setColor(0,0,0);
	love.graphics.circle("fill",25,25,21);
	if (Play.choosingState) {
		love.graphics.setColor(74,121,255);
	}
	else {
		love.graphics.setColor(255,121,255);
	}
	love.graphics.rotate(-Math.PI/2);
	love.graphics.arc("fill",-25,25,20,(Math.PI*2)*this.time,0)
	love.graphics.pop();

	if (!this.openingCurtains) {
		love.graphics.setColor(0,0,0);
		love.graphics.print("Click",20,16);
		love.graphics.print("anywhere",12,23);
		love.graphics.print("to start",14,31);
	}

	love.graphics.setColor(0,0,0,this.fadeout*255);
	love.graphics.rectangle("fill",0,0,400,400);

}

Play.setStateShower = function (obj) {
	Play.whoShowStates = obj;
	this.buttons = Group.new();
	for (var i = 0; i < Play.whoShowStates.states.length; i++) {
		var s = Button.new(75,30+18*i);
		s.width = 172;
		s.height = 15;
		s.setFunction(this,"doAction",[obj,i]);
		this.buttons.add(s);
	}
	var s = Button.new(75,30+18*Play.whoShowStates.states.length);
	s.setFunction(this,"exitButtons");
	s.width = 172;
	s.height = 15;
	this.buttons.add(s);

}

Play.doAction = function (obj,i) {
	Play.clickedSomething = true;
	if (obj.states[i].substring(0,1) == "*") {
		if (obj.selectedState == i) {
			obj.anim = obj.oldState;
			obj.selectedState = null;
		}
		else {
			// obj.choseState = true;
			obj.selectedState = i;
			obj.anim = obj.stateEffects[i]
			// print(obj.anim);
			obj.playing = true;
		}
	}
	else {
		if (obj.selectedText == i) {
			obj.selectedText = null;
			obj.textObj.text = null;
		}
		else {
			// obj.choseText = true;\
			obj.selectedText = i;
			obj.textObj.x = -23;
			obj.textObj.text = obj.states[i];
		}
	}
}

Play.exitButtons = function () {
	this.buttons = null;
	Play.whoShowStates = null;
	Play.clickedExit = true;
}

Play.nextStep = function (s) {
	if (s) {
		this.corrects++;
		this.good.alpha = 1;
		this.good.scale.x = 1;
		this.good.scale.y = 1;
	}
	else {
		this.bad.alpha = 1;
		this.bad.scale.x = 1;
		this.bad.scale.y = 1;
		this.errors++;
	}
	for (var key in this.actors) {
		this.actors[key].draw();
		var s = this.actors[key];
		s.textObj.text = null;
		s.selectedText = -1;
		s.selectedState = -1;
	}
}

Play.startGame = function () {
	this.audio.play();
	this.openingCurtains = true;
	Tween.to(this.curtainsLeft,2,{x:-200}).easing("in","quad");
	Tween.to(this.curtainsRight,2,{x:350}).easing("in","quad").onComplete( function (x) { Play.gameStarted = true });
}

Play.closeCurtains = function () {
	this.items = Group.new();
	this.audio.stop();
	for (var i = 0; i < this.corrects; i++) {
		var flower = Entity.new(Utils.random(30,290),600);
		flower.angle = Utils.random(Math.PI*2);
		flower.setImage("images/flower.png");
		Tween.to(flower,1.3,{y:182+Math.random()}).delay(Math.random()).easing("out","back");
		this.items.add(flower);
	}


	for (var i = 0; i < this.errors; i++) {
		var tomato = Entity.new(Utils.random(30,290),600);
		tomato.angle = Utils.random(Math.PI*2);
		tomato.setImage("images/tomato.png",16,16);
		Tween.to(tomato,0.7,{y:80+Math.random()*80}).delay(Math.random()).onComplete(function (self) { self.frame = 1; }).easing("out","back");
		this.items.add(tomato);
	}


	Tween.to(this.script,1,{y:400});
	Tween.to(this.curtainsLeft,2,{x:0}).easing("out","back");
	Tween.to(this.curtainsRight,2,{x:150}).easing("out","back");
	Tween.to(this,1,{fadeout:1}).delay(3).onComplete(function () {game.toReview()});
}