Review = Entity.extend("Review");

Review.init = function (level,rating) {
	this.audio = love.audio.newSource("audio/review.wav");
	this.audio.setLooping(true);
	this.audio.setVolume(0.5);
	this.audio.play();
	Review.super.init(this,0,0);
	this.fadeOut = 1;
	Tween.to(this,1,{fadeOut : 0});
	this.waitTimer = 2;
	this.setImage("images/review.png",320,211);
	this.frame = level;
	this.level = level;
	if (level == 2) {
		this.title = "- Cowboy Gun -"
		if (rating == 5) {
		this.text = "It is without question that this act was phenomenal. \
Thanks to the great performance by Dave, we will give this show 5 stars! \
Also the dying scene by Billy was looking extremely realistic. \
They even had to call an ambulance after the show! Now THAT is what I call acting! \
All by all, this show is definitely worth your money.";
this.facts = "Hollywood already has plans for making this show into a movie! It will be directed by Daniël Haazen, famous for filming movies in one take."

}
else if (rating == 4) {
			this.text = "Now let's get this straight. This director definitely knows his shit. \
When looking at this performance, we see talent all over the place. \
But this talent doesn't come without errors. Dave and Billy are both great actors, but as soon as one of them fails, \
it damages the act of the other as well. Something that was very clear here. \
Therefore we have to give it 4 stars.";

this.facts = "Hollywood is thinking about making this show into a movie! It would be directed by Daniël Haazen, famous for filming movies in one take."

}

else if (rating == 3) {
			this.text = "I'm not going to lie. I had fun. I enjoyed it. \
But sometimes things just didn't make sense. I'm not sure if Dave forgot one of his lines, \
or Billy said one of his too early, but something went definitely wrong there. \
In any case, even with the errors, I was entertained. This show will get 3 stars.";

this.facts = "Hollywood has its eye about making this show into a movie! Daniël Haazen is considering directing it, the director famous for filming movies in one take."

}

else if (rating == 2) {
	this.text = "Ouch. I'm not sure what this director has been doing these past 8 months, \
but it must've been something else than directing.\
Most of the time, the actors said things that seemed to be completely on the wrong moment.\
I couldn't follow the story at all, if there was any.\
Is this the new form of art people have been talking about?    \
That 'art' will get 2 stars. ";


this.facts = "Hollywood thought about making this into a movie. But after Daniël Haazen seeing the show (the director famous for filming movies in one take), decided not to."
}
else if (rating == 1) {
	this.text = "I had to hold in my laugh in the theater. What a joke. \
This director has no idea what he is doing. \
Does he even know that you can press escape to exit the actor's menu? \
Or that he can click directly on another actor instead of clicking - CLOSE - first? \
I'm even starting to doubt if he knew about being able to click on the script to reread it. \
1 star, for trying.";


this.facts = "This performance. Lol.";

}

	}


else if (level == 1) {
		this.title = "- Romeo and Juliet -"
		if (rating == 5) {
		this.text = "It is without question that this act was phenomenal. \
Thanks to the great performance by Dave, we will give this show 5 stars! \
Also the scene with Billy showing his fear looked extremely realistic. \
He even needed help getting from the stage! Now THAT is what I call acting! \
All by all, this show is definitely worth your money.";
this.facts = "Juliet is played by a guy.";

}
else if (rating == 4) {
			this.text = "Now let's get this straight. This director definitely knows his shit. \
When looking at this performance, we see talent all over the place. \
But this talent doesn't come without errors. Dave and Billy are both great actors, but as soon as one of them fails, \
it damages the act of the other as well. Something that was very clear here. \
Therefore we have to give it 4 stars.";

this.facts = "Billy and Dave are brothers.";

}

else if (rating == 3) {
			this.text = "I'm not going to lie. I had fun. I enjoyed it. \
But sometimes things just didn't make sense. I'm not sure if Dave forgot one of his lines, \
or Billy said one of his too early, but something went definitely wrong there. \
In any case, even with the errors, I was entertained. This show will get 3 stars.";

this.facts = "Dave is transvestite.";

}

else if (rating == 2) {
	this.text = "Ouch. I'm not sure what this director has been doing these past 8 months, \
but it must've been something else than directing.\
Most of the time, the actors said things that seemed to be completely on the wrong moment.\
I couldn't follow the story at all, if there was any.\
Is this the new form of art people have been talking about?    \
That 'art' will get 2 stars. ";


this.facts = "Billy misses 1 nipple";
}
else if (rating == 1) {
	this.text = "I had to hold in my laugh in the theater. What a joke. \
This director has no idea what he is doing. \
Does he even know that you can press escape to exit the actor's menu? \
Or that he can click directly on another actor instead of clicking - CLOSE - first? \
I'm even starting to doubt if he knew about being able to click on the script to reread it. \
1 star, for trying.";


this.facts = "This performance. Lol.";

}
}

else if (level == 0) {
		this.title = "- Waiting for Godot -"
		if (rating == 5) {
		this.text = "It is without question that this act was phenomenal. \
Thanks to the great performance by Dave, we will give this show 5 stars! \
Also the scene with Billy showing his boredom looked extremely realistic. \
All by all, this show is definitely worth your money.";
this.facts = "Godot was played by Steve.";

}
else if (rating == 4) {
			this.text = "Now let's get this straight. This director definitely knows his shit. \
When looking at this performance, we see talent all over the place. \
But this talent doesn't come without errors. Dave and Billy are both great actors, but as soon as one of them fails, \
it damages the act of the other as well. Something that was very clear here. \
Therefore we have to give it 4 stars.";

this.facts = "Billy and Dave are brothers.";

}

else if (rating == 3) {
			this.text = "I'm not going to lie. I had fun. I enjoyed it. \
But sometimes things just didn't make sense. I'm not sure if Dave forgot one of his lines, \
or Billy said one of his too early, but something went definitely wrong there. \
In any case, even with the errors, I was entertained. This show will get 3 stars."

this.facts = "Billy is afraid of heights."

}

else if (rating == 2) {
	this.text = "Ouch. I'm not sure what this director has been doing these past 8 months, \
but it must've been something else than directing.\
Most of the time, the actors said things that seemed to be completely on the wrong moment.\
I couldn't follow the story at all, if there was any.\
Is this the new form of art people have been talking about?    \
That 'art' will get 2 stars. ";


this.facts = "Dave misses both big toes.";
}
else if (rating == 1) {
	this.text = "I had to hold in my laugh in the theater. What a joke. \
This director has no idea what he is doing. \
Does he even know that you can press escape to exit the actor's menu? \
Or that he can click directly on another actor instead of clicking - CLOSE - first? \
I'm even starting to doubt if he knew about being able to click on the script to reread it. \
1 star, for trying.";


this.facts = "This performance. Lol.";

}

}
else if (this.level == 3) {
	this.title = "- Featured Director -"
	this.text = "This director is famous for directing 3 shows this year. \
Starting with Godot, after which came Romeo and Juliet and he finished it off with Cowboy Gun! \
Based on these 3 show the director won the following amount of points: ";
this.facts = "This game was made for Ludum Dare in 48 hours by Sheepolution / Daniël Haazen.       I hope you enjoyed it!"
}


}


Review.update = function () {
	this.waitTimer -= dt;
	if (this.waitTimer < 0) {
		if (Input.mousePressed("l")) {
			this.waitTimer = 1000;
			Tween.to(this,1,{fadeOut : 1}).onComplete ( function (self) {if (self.level == 2) { self.init(3); } else if (self.level == 3) { self.audio.stop(); game.init() } else {self.audio.stop(); game.toNextLevel(); }} );
		}
	}
}


Review.draw = function () {
	Review.super.draw(this);
	love.graphics.setColor(100,100,100);
	love.graphics.setFont(Game.fonts.name);
	love.graphics.printf(this.title,185,80,120,"center");
	love.graphics.setFont(Game.fonts.script);
	love.graphics.printf(this.text,130,90,120,"left");
	love.graphics.print("FUN FACTS",130,158);
	love.graphics.printf(this.facts,130,165,120,"left");
	if (this.level == 3) {
		love.graphics.setFont(Game.fonts.title);
		love.graphics.print(Game.points + "/22",130,137);
	}
	love.graphics.setColor(0,0,0,255*this.fadeOut);
	love.graphics.rectangle("fill",0,0,400,400);
}