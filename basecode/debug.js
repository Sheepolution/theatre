Debug = Object.extend("Debug");

Debug.font = love.graphics.newFont("arial",10),
Debug.entities = [];
Debug.active = false;
Debug.selected;
Debug.scroll = 0;
Debug.speed = 1;
Debug.direction = 1;
Debug.search = "";
Debug.searchActive = false;
Debug.multiply = false;
Debug.entitiesOnScreen = 0;
Debug.fps = {
		startTime : 0,
		frameNumber : 0,
		getFPS : function(){
			this.frameNumber++;
			var d = new Date().getTime(),
				currentTime = ( d - this.startTime ) / 1000,
				result = Math.floor( ( this.frameNumber / currentTime ) );

			if (currentTime > 1) {
				this.startTime = new Date().getTime();
				this.frameNumber = 0;
			}
			return result;
		}
	}	

Debug.update = function () {
	if (love.keyboard.isDown("shift") && Input.keyPressed(" ")) {
		Debug.active = !Debug.active;
		return
	}
	if (Debug.active) {
		Debug.entitiesOnScreen = 0;
		var scrn = Rect.new(0,0,love.graphics.getWidth(),love.graphics.getHeight());
		for (var i=0; i < Debug.entities.length; i++) {
			if (scrn.overlaps(Debug.entities[i])) {
				Debug.entitiesOnScreen++;
			}
		}
		if (Input.mousePressed("wd")) {
			Debug.scroll += 13;
		}
		else if (Input.mousePressed("wu")) {
			Debug.scroll -= 13;
		}
		Debug.scroll = Utils.clamp(Debug.scroll,0,800);
		if (love.keyboard.isDown("shift")) {
			if (Input.keyPressed(".")) {
				return;
			}
			else if (Input.keyPressed(",")) {
				dt = -dt;
				return;
			}
			if (Input.keyPressed("f")) {
				if (Debug.selected!=null) {
					Debug.searchActive = !Debug.searchActive;
					return
				}
			}
			if (Input.mousePressed("l")) {
				var succes;
				var p = Rect.new(love.mouse.getX(),love.mouse.getY());
				for (var i=0; i < Debug.entities.length; i++) {
					if (Debug.entities[i]!=Debug.selected) {
						if (Debug.entities[i].overlaps(p)) {
							succes = Debug.entities[i];
							break
						}
					}
				}
				Debug.selected = succes;
			}
			for (var i=0; i < 10; i++) {
				if (Input.keyPressed(i)) {
					if (i == 0) {
						Debug.speed = 0;
					}
					else {
						Debug.speed = 1/i;
					}
					break;
				}
			}
			if (Input.keyPressed("-")) {
				Debug.direction = -Debug.direction;
			}
			if (Input.keyPressed("=")) {
				Debug.multiply = !Debug.multiply;
			}

			
		}
		if (Debug.searchActive) {
			if (Input.keyPressed("backspace")) {
				Debug.scroll = 0;
				Debug.search = Debug.search.substring(0,Debug.search.length-1);
			}
			for (var i=0; i < Input.keyspressed.length; i++) {
				if (Input.keyspressed[i].length == 1) {
					Debug.scroll = 0;
					Debug.search = Debug.search + Input.keyspressed[i];
				}
			}
		}
		if (Debug.multiply) {
			if (Debug.speed == 0) {
				dt = 0;
			}
			else {
				dt /= Debug.speed * Debug.direction;
			}
		}
		else {
			
			dt *= Debug.speed * Debug.direction;
		}
	}
}

Debug.draw = function () {
	love.graphics.setFont(Debug.font);
	if (Debug.active) {
		for (var i=0; i < Debug.entities.length; i++) {
			var obj = Debug.entities[i];
			if (obj.drawDebug) {
				obj.drawDebug();
			}
		
			// for (var prop in obj) {
			// 	if (Array.isArray(obj[prop]) || typeof(obj[prop])!="function" && typeof(obj[prop])!="object") {
			// 		love.graphics.printf(prop + " = " + obj[prop],obj.right()+10,obj.top()+13*j-Debug.scroll,250,"left");
			// 		j++;
			// 	}
			// 	else if (typeof(obj[prop]=="object")) {
			// 		for (var p in obj[prop]) {
			// 			var probj = obj[prop];
			// 			if (typeof(probj[p])!="function" && typeof(probj[p])!="object") {
			// 				love.graphics.printf(prop + "." + p + " = " + probj[p],obj.right()+10,obj.top()+13*j-Debug.scroll,250,"left");
			// 				j++;
			// 			}
			// 		}
			// 	}
			// }
		}
		// print(Debug.selected);

		if (Debug.selected) {
			var j = 0;
			var obj  = Debug.selected;
			if (Debug.searchActive) {
				love.graphics.setColor(0,0,0);
				love.graphics.rectangle("fill",obj.right(),obj.top()-13,250,13);
				love.graphics.setColor(255,255,255);
				love.graphics.print("Search: " + Debug.search,obj.right()+10,obj.top()-13);
			}
			love.graphics.setScissor(obj.right(),obj.top(),250,Math.max(105,obj.height));
			love.graphics.setColor(0,0,0);
			love.graphics.rectangle("fill",obj.right(),obj.top(),250,Math.max(105,obj.height))
			love.graphics.setColor(255,255,255);
			for (var prop in obj) {
				if (Array.isArray(obj[prop]) || typeof(obj[prop])!="function" && typeof(obj[prop])!="object") {
					if (prop.indexOf(Debug.search)!=-1) {
						var value = typeof(obj[prop]) == "number" ? Math.floor(obj[prop]*100)/100 : obj[prop];
						love.graphics.printf(prop + " = " + value,obj.right()+10,obj.top()+13*j-Debug.scroll);
						j++;
					}
				}
				else if (typeof(obj[prop]=="object")) {
					for (var p in obj[prop]) {
						if (p.indexOf(Debug.search)!=-1 || prop.indexOf(Debug.search)!=-1) {
							var probj = obj[prop];
							if (typeof(probj[p])!="function" && typeof(probj[p])!="object") {
								var value = typeof(probj[p]) == "number" ? Math.floor(probj[p]*100)/100 : probj[p];
								love.graphics.printf(prop + "." + p + " = " + value,obj.right()+10,obj.top()+13*j-Debug.scroll);
								j++;
							}
						}
					}
				}
			}
		}
		love.graphics.setScissor();
		love.graphics.setColor(0,0,0);
		love.graphics.rectangle("fill",0,0,110,100);
		love.graphics.setColor(255,255,255);
		love.graphics.print("FPS: " + Debug.fps.getFPS(),5,5);
		love.graphics.print("Entities: " + Debug.entitiesOnScreen + "/" + Debug.entities.length,5,18);
		love.graphics.print("--Debug settings--",5,44);
		if (Debug.multiply) {
			var spd = 1 / Debug.speed;
			spd = spd == Infinity ? 0 : spd;
			love.graphics.print("speed: " + (spd*Debug.direction),5,57);
		}
		else {
			love.graphics.print("speed: " + Math.floor((Debug.speed * Debug.direction)*100)/100,5,57);
		}
	}
	// love.graphics.circle("fill",love.mouse.getX(),love.mouse.getY(),3,10)
}

Debug.add = function (entity) {
	Debug.entities.push(entity);
}