Point = Object.extend("Point");

Point.init = function (x,y) {
	this.x = x || 0;
	this.y = y || 0;
}

Point.set = function (x,y) {
	this.x = x || this.x;
	this.y = y || this.y;
}

Point.clone = function (r) {
	this.x = r.x;
	this.y = r.y;
}

Point.distanceX = function (r) {
	return Math.abs(this.x - r.x);
}

Point.distanceY = function (r) {
	return Math.abs(this.y - r.y)
}