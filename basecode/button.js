Button = Entity.extend("Button");

Button.init = function (x,y,clickable,shape) {
	Button.super.init(this,x,y);
	this.clickable = clickable || true;
	this.shape = shape || "rectangle";
	this.states = {};
	this.currentState;
	this.mouse = Rect.new(0,0,0,0);
}


Button.update = function () {
	this.anim = "idle";
	if (this.clickable) {
		if (this.mouseOver()) {
			this.anim = love.mouse.isDown("l") ? "down" : "hover";

			if (this.onRelease) {
				if (Input.mouseReleased("l")) {
					this.obj[this.func].apply(this.obj,this.args);
					return
				}
			}
			else if (this.obj) {
				if (Input.mousePressed("l")) {
					this.obj[this.func].apply(this.obj,this.args);
					return
				}
			}
		}
	}

	Button.super.update(this);
	if (this.currentState) {
		this.frame += this.states[this.currentState];
	}
}


Button.mouseOver = function() {
	this.mouse.set(mouseX,mouseY,3,3);
	this.mouse.middleX(this.mouse.x);
	this.mouse.middleY(this.mouse.y);
	if (this.shape == "circle") {
		if (this.distance(this.mouse) < this.width/2) {
			love.mouse.setCursor("pointer");
			return true;
		}
	}
	else if (this.shape == "rectangle") {
		if (this.overlaps(this.mouse)) {
			love.mouse.setCursor("pointer");
			return true;
		}
	}
	return false;
}

Button.setFunction = function(obj,func,args,onRelease) {
	this.obj = obj;
	this.func = func;
	this.args = args || null;
	this.onRelease = onRelease || false;
}

Button.addState = function(name,value) {
	this.states[name] = value;
	this.currentState = name;
}

Button.setState = function(name) {
	this.currentState = name;
}