Rect = Point.extend("Rect");

Rect.init = function (x,y,width,height) {
	Rect.super.init(this,x,y);
	this.width = width || 0;
	this.height = height || this.width;
}

Rect.set = function (x,y,width,height) {
	this.x = x || this.x;
	this.y = y || this.y;
	this.width = width || this.width;
	this.height = height || this.height;
}

Rect.draw = function (v) {
	love.graphics.rectangle(v,this.x,this.y,this.width,this.height);
}

Rect.clone = function (r) {
	this.x = r.x;
	this.y = r.y;
	this.width = r.width;
	this.height = r.height;
}

Rect.left = function (v) {
	if (v) { this.x = v; }
	return this.x;
}

Rect.right = function (v) {
	if (v) { this.x = v - this.width; }
	return this.x + this.width;
}

Rect.top = function (v) {
	if (v) { this.y = v; }
	return this.y;
}

Rect.bottom = function (v) {
	if (v) { this.y = v - this.height; }
	return this.y + this.height;
}

Rect.middleX = function (v) {
	if (v) { this.x = v - this.width/2; }
	return this.x + this.width/2; 
}

Rect.middleY = function (v) {
	if (v) { this.y = v - this.height/2; }
	return this.y + this.height/2;
}

Rect.overlaps = function (r) {
	return this.x + this.width > r.x && this.x < r.x + r.width
	&& this.y + this.height > r.y && this.y < r.y + r.height
}

Rect.overlapsX = function (r) {
	return this.x + this.width > r.x && this.x < r.x - r.width;
}

Rect.overlapsY = function (r) {
	return this.y + this.height > r.y && this.y < r.y - r.height;
}

Rect.distanceX = function (r) {
	return Math.abs(this.middleX() - r.middleX());
}

Rect.distanceY = function (r) {
	return Math.abs(this.middleX() - r.middleX());
}

Rect.distance = function (r) {
	return Math.sqrt(Math.pow(this.middleX() - r.middleX(),2) + Math.pow(this.middleY() - r.middleY(),2));
}