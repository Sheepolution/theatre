Group = Object.extend("Group");

Group.init = function () {
	this.members = [];
}

Group.add = function (obj) {
	if (arguments.length > 1) {
		for (var i = 0; i < arguments.length; i++) {
			this.members.push(arguments[i]);
			for (key in arguments[i]) {
				if (!this.hasOwnProperty(key)) {
					if (typeof(arguments[i][key]) == "function") {
						this.makeFunc(key);
					}
				}
			}
		}
	}
	else if (obj.is(Group)) {
		for (var i=0; i < obj.members.length; i++) {
			this.members.push(obj.members[i]);
			for (key in obj.members[i]) {
				if (!this.hasOwnProperty(key)) {
					if (typeof(obj.members[i][key]) == "function") {
						this.makeFunc(key);
					}
				}
			}
		}
	}
	
	else {
		this.members.push(obj);
		for (key in obj) {
			if (!this.hasOwnProperty(key)) {
				if (typeof(obj[key]) == "function") {
					this.makeFunc(key);
				}
			}
		}
	}
}

Group.remove = function (obj) {
	if (typeof(obj) == "object") {
		var dead;
		for (var i=0; i < this.members.length; i++) {
			if (this.members[i] == obj) {
				dead = i;
				break;
			}
		}
		this.members.splice(dead,1);
	}
	else {
		this.members.splice(i,1);
	}
	
}


Group.makeFunc = function (k) {
	this[k] = function () {
		if (arguments[0] == true) {
			for (var i=0; i < this.members.length-1; i++) {
				for (var j=i; j < this.members.length; j++) {
					if (i!=j) {
						if (this.members[i].hasOwnProperty(k) && this.members[j].hasOwnProperty(k)) {
							arguments[0] = this.members[j];
							var a = this.members[i][k].apply(this.members[i],arguments);
							if (a) {
								break;
							}
							arguments[0] = this.members[i];
							var b = this.members[j][k].apply(this.members[j],arguments);
							if (b) {
								break;
							}
						}
					}
				}
			}
		}
		else {
			for (var i=0; i < this.members.length; i++) {
				if (this.members[i].hasOwnProperty(k)) {
					this.members[i][k].apply(this.members[i],arguments);
				}
			}
		}
		
	}
}

Group.do = function (f) {
	for (var i=0; i < this.members.length; i++) {
		f(this.members[i]);
	}
}

Group.onTop = function (i) {
	if (typeof(i) == "number") {
		var obj = this.members[i];
		this.members.splice(i,1);
		this.members.push(obj);	
	}
	else {
		var numb = Utils.find(this.members,i);
		this.members.splice(numb,1);
		this.members.push(i);
	}
	
}

Group.count = function (f) {
	return Utils.count(this.members,f);
}