//Thanks rxi!
Entity = Rect.extend("Entity");

Entity.imageCache = {};
Entity.audioCache = {};

Entity.init = function (x,y,w,h) {
	Entity.super.init(this,x,y,w,h);
	this.last = Rect.new(x,y,w,h);
	this.origin = Point.new();
	this.offset = Point.new();
	this.velocity = Point.new();
	this.maxVelocity = Point.new(99999,99999);
	this.accel = Point.new();
	this.drag = Point.new();
	this.bounce = Point.new();

	this.separatePriority = 0;
	this.solid = true;

	this.scale = Rect.new(1,1);
	this.color = [255,255,255];
	this.alpha = 1;
	this.flip = false;
	this.angle = 0;
	this.rotation = 0;

	this.anim = "idle";
	this.currentAnim = "idle";
	this.playing = true;
	this.frame = 0;
	this.frameTimer = 0;
	this.frameTimerDir = 1;
	this.anims = {};
	this.ended = true
	Debug.add(this);

}

Entity.update = function () {
	this.animate();
	this.updateMovement();
	this.angle += this.rotation * dt;
}

Entity.draw = function () {
	love.graphics.setColor(this.color[0],this.color[1],this.color[2],this.alpha*255);
	love.graphics.draw(this.image,this.frames[this.frame],this.x+this.offset.x+this.origin.x,this.y+this.offset.y+this.origin.y,this.angle,this.flip ? -this.scale.x : this.scale.x,this.scale.y,this.origin.x,this.origin.y);
	love.graphics.setColor(255,255,255,255);
}
Entity.drawDebug = function () {
	var lw = love.graphics.getLineWidth();
	var cl = love.graphics.getColor();
	love.graphics.setLineWidth(2);
	love.graphics.setColor(0,255,0,255)
	love.graphics.rectangle("line",this.x,this.y,this.width,this.height);
	love.graphics.setColor(255,255,255,255)
	love.graphics.circle("fill",this.x+this.origin.x,this.y+this.origin.y,5,25);
	love.graphics.setLineWidth(lw);
	love.graphics.setColor(cl);
}


Entity.centerOrigin = function () {
	this.origin.x = this.width/2;
	this.origin.y = this.height/2;
}

Entity.setImage = function (url,width,height) {
	if (!Entity.imageCache[url]) {
		Entity.imageCache[url] = love.graphics.newImage(url);
		// Entity.imageCache[url].setFilter("nearest");
	}
	this.image = Entity.imageCache[url];
	this.width = width || this.image.getWidth();
	this.height = height || this.image.getHeight();
	this.frames = [];
	for (var i=0; i < this.image.getHeight()/this.height; i++) {
		for (var j=0; j < this.image.getWidth()/this.width; j++) {
			this.frames.push(love.graphics.newQuad(j*this.width,i*this.height,this.width,this.height));
		}
	}
	this.centerOrigin();
}

Entity.addAnimation = function (name,start,finish,speed,mode,semi) {
	this.anims[name] = {};

	
	this.anims[name].speed = speed || 15;
	this.anims[name].mode = mode || "loop";
	if (start >= finish) {
		this.anims[name].start = finish-1;
		this.anims[name].finish = start-1;
		this.anims[name].direction = -1;
	}
	else {
		this.anims[name].start = start-1;
		this.anims[name].finish = finish-1;
		this.anims[name].direction = 1;
	}
	if (semi==null) {
		this.anims[name].semi = start-1;
	}
	else {
		this.anims[name].hasSemi = true;
		this.anims[name].semi = semi-1;
	}
		
}


Entity.checkAnim = function () {
	if (this.currentAnim != this.anim) {
		this.currentAnim = this.anim;
		this.frameTimer = this.anims[this.anim].start;
		this.frameTimerDir = this.anims[this.anim].direction;
		this.ended = false;
		this.playing = true;
	}
}


Entity.animate = function () {
	if (this.playing) {
		if (this.anims.hasOwnProperty(this.anim)) {
			this.checkAnim();
			if (this.anims[this.anim].start == this.anims[this.anim].finish) {
				this.frameTimer = this.anims[this.anim].start;
				this.frame = this.frameTimer;
				return;
			}
			if (typeof(this.anims[this.anim].speed) == "number") {
				this.frameTimer += dt * this.anims[this.anim].speed * this.frameTimerDir;
			}
			else {
				this.frameTimer += dt * this.anims[this.anim].speed[this.frame] * this.frameTimerDir;
			}
			if (this.frameTimer > this.anims[this.anim].finish+1 || this.frameTimer < this.anims[this.anim].start) {
				if (this.anims[this.anim].mode == "loop") {
					this.frameTimer = this.anims[this.anim].semi;
					if (this.anims[this.anim].hasSemi) {
						this.ended = true;
					}
				}
				else if (this.anims[this.anim].mode == "once") {
					this.frameTimer = this.anims[this.anim].finish;
					this.playing = false;
					this.ended = true;
				}
				else if (this.anims[this.anim].mode == "pingpong") {
					this.frameTimer = this.frameTimeDir > 0 ? this.anims[this.anim].finish-1 : this.anims[this.anim].start+1;
					this.frameTimerDir = -this.frameTimerDir;
				}
			}
			this.frame = Math.floor(this.frameTimer);
		}
	}
}

Entity.play = function () {
	this.playing = true;
}

Entity.pause = function () {
	this.playing = false;
}

Entity.stop = function () {
	this.checkAnim();
	this.playing = false;
	this.setFrame(1);
	this.ended = false;
}

Entity.restart = function (s) {
	this.checkAnim();
	this.playing = true;
	this.setFrame(1);
	this.ended = false;
}

Entity.hasEnded = function () {
	this.checkAnim();
	return this.ended;
}

Entity.isPlaying = function () {
	this.checkAnim();
	return this.playing;
}

Entity.getFrame = function () {
	this.checkAnim();
	return this.frame - this.anims[this.anim].start+1;
}

Entity.getTrueFrame = function () {
	this.checkAnim();
	return this.frame+1;
}

Entity.setFrame = function (f) {
	this.checkAnim();

	if (this.anims[this.anim].finish - this.anims[this.anim].start < f-1) {
		throw("There are only " + (this.anims[this.anim].finish - this.anims[this.anim].start) + " frames. Not " + f);
	}
	this.frame = this.anims[this.anim].start + f - 1;
	this.frameTimer = this.frame;
}

Entity.setTrueFrame = function (f) {
	this.checkAnim();

	this.frame = f - 1;
	this.frameTimer = this.frame;
}

Entity.playSound = function (url,volume) {
	if (!Entity.audioCache[url]) {
		Entity.audioCache[url] = love.audio.newSource(url);
	}
	volume = volume || 1;
	Entity.audioCache[url].stop();
	Entity.audioCache[url].setVolume(volume);
	Entity.audioCache[url].play();
}


Entity.updateMovement = function () {
	this.last.clone(this);

	this.velocity.x += this.accel.x * dt;
	if (Math.abs(this.velocity.x) > this.maxVelocity.x) {
		this.velocity.x = this.maxVelocity.x * (this.velocity.x > 0) ? 1 : -1;
	}

	this.x += this.velocity.x * dt;

	if (this.accel.x == 0 && this.velocity.x != 0 && this.drag.x != 0) {
		if (this.drag.x * dt > Math.abs(this.velocity.x)) {
			this.velocity.x = 0;
		}
		else {
			this.velocity.x += this.drag.x * dt * (this.velocity.x>0) ? -1 : 1;
		}
	}

	this.velocity.y += this.accel.y * dt;
	if (Math.abs(this.velocity.y) > this.maxVelocity.y) {
		this.velocity.y = this.maxVelocity.y * (this.velocity.y > 0) ? 1 : -1;
	}

	this.y += this.velocity.y * dt;

	if (this.accel.y == 0 && this.velocity.y != 0 && this.drag.y != 0) {
		if (this.drag.y * dt > Math.abs(this.velocity.y)) {
			this.velocity.y = 0;
		}
		else {
			this.velocity.y += this.drag.y * dt * (this.velocity.y>0) ? -1 : 1;
		}
	}
}

Entity.overlaps = function (e) {
	return this!= e && !this.dead && !e.dead && Entity.super.overlaps(this,e);
}

Entity.onOverlap = function (e) {
	if (this.solid && e.solid) {
		this.seperate(e);
		return true;
	} 
}


Entity.seperate = function (e) {
	this.separateAxis(e, this.last.overlapsY(e.last) ? "x" : "y");
}

Entity.separateAxis = function (e, a) {
	var s = (a == "x") ? "width" : "height";
	if (this.separatePriority >= e.separatePriority) {
		if ((e.last[a] + e.last[s] / 2) < (this.last[a] + this.last[s] / 2)) {
			e[a] = this[a] - e[s];
		}
		else {
			e[a] = this[a] + this[s];
		}
		e.velocity[a] = e.velocity[a] * -e.bounce[a];
	}
	else {
		e.separateAxis(this, a);
	}
}
