Tween = {};

Tween.list = [];

Tween.to = function (obj, rate, vars) {
	var tween = this._get();
	tween.obj = obj;
	tween.rate = 1/rate;
	tween.vars = vars;
	Tween.list.push(tween);
	return tween;
}

Tween._get = function() {
	var tween = {};
	tween._delay = 0;
	tween.inited = false;
	tween.progress = 0;
	tween.inout = "in";
	tween.easing = "linear";
	tween.number = Tween.list.length;

	tween.init = function() {
		for (var prop in this.vars) {
			this.vars[prop] = {
				start : this.obj[prop],
				diff : this.vars[prop] - this.obj[prop]
				};
		}
		this.inited = true;
	}

	tween.delay = function (x) {
		this._delay = x;
		return tween;
	}

	tween.easing = function (inout,easing) {
		this.inout = inout;
		this.easing = easing;
		return tween;
	}

	tween.onComplete = function(f,obj) {
		this.completeFunc = f;
		this.completeObj = obj || this.obj;
		return tween;
	}

	tween.stop = function () {
		Tween.remove(this.number);
	}

	tween.rush = function () {
		this.progress = 1;
	}


	return tween;
}

Tween.update = function () {
	for (var i=0; i < Tween.list.length; i++) {
		var t = Tween.list[i];
		if (!t.inited) { t.init() };
		if (t._delay > 0) { t._delay -= dt; continue};
		t.progress += t.rate * dt;
		var p = t.progress;
		p = p >= 1 ? 1 : p;
		p = this.ease(p,t.inout,t.easing);
		for (var prop in t.vars) {
			// print(t.vars[prop].start ,t.progress , t.vars[prop].diff)
			t.obj[prop] =  t.vars[prop].start + p * t.vars[prop].diff;
		}
		if (t.progress >= 1) {
			if (t.completeFunc) { t.completeFunc(t.completeObj); }
			this.remove(i);
		}
	}
}

Tween.remove = function (i) {
	this.list.splice(i,1);
}


Tween.ease = function (p,inout,easing) {
	if (inout == "out") {
		p = 1 - p;
		if (easing == "back") {
			p = 1 - this.back(p);
		}
		else if (easing == "quad") {
			p = 1 - this.quad(p);
		}
	}
	if (inout == "in") {
		if (easing == "back") {
			p = this.back(p);
		}
		else if (easing == "quad") {
			p = this.quad(p);
		}
	}
	if (inout == "inout") {
		p = p * 2 
	    if (p < 1) {
	      return .5 * (this.quad(p));
	    }
	    else
	      p = 2 - p
	      return .5 * (1 - (this.quad(p))) + .5
	    end 
	}
	return p;
}

Tween.back = function (p) {
	return p * p * (2.7 * p - 1.7);
}

Tween.quad = function (p) {
	return p*p*p*p;
}

 // v.start + x * v.diff