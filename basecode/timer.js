Timer = Object.extend("Timer");

Timer.init = function (def,loop,condition,obj,props,array) {
	this.defTime = def;
	this.timer = def;
	this.loop = loop;
	this.obj = obj;
	this.props = props;
	this.condition = condition || function () { return true; };
	this.array = array;
	this.paused = false;
	this.finished = false;
}

Timer.update = function () {
	if (!this.paused && !this.finished) {
		if (this.allConditions()) {
			this.timer -= dt;
			if (this.timer <= 0) {
				this.finished = true;
				if (this.loop) {
					this.timer = this.defTime;
					this.finished = false;
				}
				if (this.obj) {
					if (typeof(this.props) == "function") {
						this.props(this.obj);
					}
					else if (typeof(this.obj[this.props]) == "function") {
						this.obj[this.props].apply(this.obj,this.array);
					}
					else {
						for (var key in this.props) {
							this.obj[key] = this.props[key];
						}
					}
				}
				return true;
			}
		}
	}
	return false
}

Timer.pause = function () {
	this.paused = true;
}

Timer.stop = function () {
	this.paused = true;
	this.timer = this.defTime;
}

Timer.restart = function () {
	this.paused = false;
	this.finished = false;
	this.timer = this.defTime;
}

Timer.play = function () {
	this.paused = false;
}

Timer.allConditions = function () {
	var succes = true;
	for (var key in this.condition) {
		// print(this.obj[key],this.props[key]);
		if (this.obj[key] != this.condition[key]) {
			succes = false;
			break;
		}
	}
	return succes;
}