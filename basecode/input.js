Input = Object.extend("Input");

Input.keyspressed = [];
Input.keysreleased = [];
Input.buttonspressed = [];
Input.buttonsreleased = [];

Input.update = function () {
	Input.keyspressed = [];
	Input.keysreleased = [];
	Input.buttonspressed = [];
	Input.buttonsreleased = [];
}

Input.keyPressed = function () {
	return Utils.any(arguments, function (k) {return Utils.any(Input.keyspressed,k)});
}

Input.keyReleased = function () {
	return Utils.any(arguments, function (k) {return Utils.any(Input.keysreleased,k)});
}

Input.mousePressed = function () {
	return Utils.any(arguments, function (k) {return Utils.any(Input.buttonspressed,k)});
}

Input.mouseReleased = function () {
	return Utils.any(arguments, function (k) {return Utils.any(Input.buttonsreleased,k)});
}