Utils = Object.extend("Utils");

Utils.sign = function (a) {
	return a >= 0 ? 1 : -1;
}

Utils.any = function (arr,f) {
	if (typeof(f)=="function") {
		for (var i=0; i < arr.length; i++) {
			if (f(arr[i])) {
				return true;
			}
		}
	}
	else {
		for (var i=0; i < arr.length; i++) {
			if (arr[i] == f) {
				return true;
			}
		}
	}
	return false;
}

Utils.all = function (arr,f) {
	if (typeof(f)=="function") {
		for (var i=0; i < arr.length; i++) {
			if (!f(arr[i])) {
				return false;
			}
		}
	}
	else {
		for (var i=0; i < arr.length; i++) {
			if (arr[i] != f) {
				return false;
			}
		}
	}
	return true;
}

Utils.find = function (arr,f) {
	if (typeof(f)=="function") {
		for (var i=0; i < arr.length; i++) {
			if (f(arr[i])) {
				return i;
			}
		}
	}
	else {
		for (var i=0; i < arr.length; i++) {
			if (arr[i] == f) {
				return i;
			}
		}
	}
	return -1;
}

Utils.has = function (arr,f) {
	if (typeof(f)=="function") {
		for (var i=0; i < arr.length; i++) {
			if (f(arr[i])) {
				return true;
			}
		}
	}
	else {
		for (var i=0; i < arr.length; i++) {
			if (arr[i] == f) {
				return true;
			}
		}
	}
	return false
}

Utils.count = function (arr,f) {
	var c = 0;
	if (typeof(f)=="function") {
		for (var i=0; i < arr.length; i++) {
			if (f(arr[i])) {
				c++;
			}
		}
	}
	else {
		for (var i=0; i < arr.length; i++) {
			if (arr[i] == f) {
				c++;
			}
		}
	}
	return c;
}


Utils.clamp = function (a,min,max) {
	return Math.min(max,Math.max(min,a));
}

Utils.Tau = function () {
	return Math.PI*2;
}

Utils.getAngle = function (a,b,c,d) {
	if (!c) {
		return Math.atan2(b.y - a.y,b.x - a.x);
	}
	else {
		return Math.atan2(d - b, c - a);
	}
	throw("dafuq are you doing?");
}

Utils.random = function (s,e,d) {
	if (e==null) {
		return Math.floor(Math.random()*s);
	}
	else {
		return s+Math.floor(Math.random()*(e-s));
	}
}

Utils.HSV = function(h, s, v) {
    var r, g, b, i, f, p, q, t;
    if (h && s === undefined && v === undefined) {
        s = h.s, v = h.v, h = h.h;
    }
    i = Math.floor(h * 6);
    f = h * 6 - i;
    p = v * (1 - s);
    q = v * (1 - f * s);
    t = v * (1 - (1 - f) * s);
    switch (i % 6) {
        case 0: r = v, g = t, b = p; break;
        case 1: r = q, g = v, b = p; break;
        case 2: r = p, g = v, b = t; break;
        case 3: r = p, g = q, b = v; break;
        case 4: r = t, g = p, b = v; break;
        case 5: r = v, g = p, b = q; break;
    }
    return [
        Math.floor(r * 255),
        Math.floor(g * 255),
        Math.floor(b * 255),
        255
    ]
}