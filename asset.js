Asset = Object.extend("Asset");

Asset.init = function (x,y,img) {
	this.x = x;
	this.y = y;
	this.image = love.graphics.newImage(img);
}

Asset.draw = function () {
	love.graphics.setColor(255,255,255,255);
	love.graphics.draw(this.image,this.x,this.y);
}