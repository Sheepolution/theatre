Menu = Object.extend("Menu");

Menu.init = function () {
	this.sheepolution = love.graphics.newImage("images/sheepolution.png");
	this.fadeout = 1;
	Tween.to(this,0.3,{fadeout : 0}).delay(2);
	this.background = love.graphics.newImage("images/stage_back.png");
	this.background2 = love.graphics.newImage("images/stage_front.png");
	this.logo = love.graphics.newImage("images/logo.png");
	this.buttonStart = Button.new(110,80,true,"rectangle");
	this.buttonStart.width = 100;
	this.buttonStart.height = 30;
	this.buttonStart.setFunction(this,"startGame");

	this.buttonHelp = Button.new(110,130);
	this.buttonHelp.width = 100;
	this.buttonHelp.height = 30;
	this.buttonHelp.setFunction(this,"showHelp")

	this.currentHelp = 0;
	this.helps = [
		love.graphics.newImage("tut/script.png"),
		love.graphics.newImage("tut/actors.png"),
		love.graphics.newImage("tut/text.png"),
		love.graphics.newImage("tut/timer.png")
	]

	this.showingHelp = false;

}

Menu.update = function () {
	if (this.fadeout == 0) {
		if (this.showingHelp) {
			if (Input.mousePressed("l")) {
				this.showingHelp = false
			}
		}
		else {
			this.buttonStart.update();
			this.buttonHelp.update();
		}
	}
}

Menu.draw = function () {
	love.graphics.draw(this.background);
	love.graphics.draw(this.background2);
	love.graphics.scale(1/2);
	love.graphics.draw(this.logo,100,10);
	love.graphics.scale(2);
	love.graphics.setColor(0,0,0);
	love.graphics.rectangle("fill",110,80,100,30);


	love.graphics.rectangle("fill",110,130,100,30);


		love.graphics.setColor(255,255,255);
		love.graphics.setFont(Game.fonts.name);
		love.graphics.print("START",138,90,0,2,2);
		love.graphics.print("HELP",143,140,0,2,2);
		love.graphics.setColor(0,0,0);
	if (this.showingHelp) {
		love.graphics.rectangle("fill",0,0,400,400);
		love.graphics.setColor(255,255,255);
		love.graphics.printf("This is the script. After every line is a sequence.",15,1,100,"left")
		love.graphics.printf("These are actors. You can click on them to make them do stuff.",200,1,100,"left")
		love.graphics.printf("Blue is an action, yellow is a text.",10,115,200,"left")
		love.graphics.printf("This shows how much time you have left.",200,115,100,"left")
		love.graphics.setColor(255,0,0)
		love.graphics.printf("Make actors do stuff along the script.",80,90,300,"left")
		love.graphics.scale(1/3);
		love.graphics.draw(this.helps[0],50,50);
		love.graphics.draw(this.helps[1],600,75);
		love.graphics.draw(this.helps[2],10,400);
		love.graphics.draw(this.helps[3],660,400);
	}
	love.graphics.setColor(0,0,0,255*this.fadeout);
	love.graphics.rectangle("fill",0,0,400,400);
	love.graphics.scale(1/3);
	love.graphics.draw(this.sheepolution,170,0);
	love.graphics.setColor(0,0,0,255);
}

Menu.startGame = function () {
	game.toNextLevel();
}

Menu.showHelp = function () {
	this.showingHelp = true;
}