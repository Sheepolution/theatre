Game = Object.extend("Game");

Game.fonts = {
	script : love.graphics.newFont("script",3.4),
	name : love.graphics.newFont("script",5),
	title : love.graphics.newFont("script",12)
}

Game.points = 0;


Game.init = function () {
	Game.points = 0;
	this.state = "menu";
	this.level = -1;
	this.menu = Menu.new();
	this.levels = [Godot,Romeo,Cowboys];
	// this.play = this.levels[this.level].new();
	// this.review = Review.new(3,4);
}

Game.update = function () {
	if (this.state == "play") {
		this.play.update();
	}
	else if (this.state == "review") {
		this.review.update();
	}
	else if (this.state == "menu") {
		this.menu.update();
	}
}

Game.draw = function () {
	if (this.state == "play") {
		this.play.draw();
	}
	else if (this.state == "review") {
		this.review.draw();
	}
	else if (this.state == "menu") {
		this.menu.draw();
	}
}

Game.toReview = function () {
	this.state = "review";
	Game.points += this.play.corrects;
	var score = this.play.corrects;
	if (this.play.corrects > 0) {
		score = score / (score+this.play.errors)
	}
	else {
		score = 0;
	}
	
	score *= 5;
	print("voor matfloor",score);
	score = Math.ceil(score);
	print("daarna",score);
	this.play = null;
	this.review = Review.new(this.level,score);
}

Game.toNextLevel = function  () {
	this.level++;
	this.play = this.levels[this.level].new();
	this.review = null;
	this.state = "play";
}