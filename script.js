Script = Object.extend("Script");

Script.init = function () {
	this.x = 22;
	this.y = 22;
	this.limit = 120;
	this.showing = true;
	this.img = love.graphics.newImage("images/script.png");
	
	this.button = Button.new(82,180,true,"rectangle");
	this.button.width = 160;
	this.button.height = 300;
	this.button.centerOrigin();
	this.button.setFunction(this,"show");
	this.tween;
}

Script.update = function () {
	this.button.y = this.y;
	this.button.update();
}

Script.draw = function () {
	love.graphics.draw(this.img,this.x,this.y);
	love.graphics.setFont(Game.fonts.title);
	love.graphics.setColor(80,80,80);
	love.graphics.print("SCRIPT",this.x + 110,this.y + 16);
	love.graphics.setFont(Game.fonts.script);
	love.graphics.printf("Cowboy: You crimed your last crime!",112,this.y + 40,this.limit,"left");
	love.graphics.line(112,this.y+50,222,this.y+50);
	love.graphics.printf("Bandit: Sorry Sheriff, but my next crime is already scheduled!",112,this.y+60,this.limit,"left");
	love.graphics.line(112,this.y+80,222,this.y+80);
	love.graphics.printf("Cowboy: I can't let you do that, Bandit!",112,this.y+90,this.limit,"left");
	love.graphics.line(112,this.y+100,222,this.y+100);
	love.graphics.printf("*Both the Bandit and the Cowboy grab their guns and point to each other*",112,this.y+110,this.limit,"left");
	love.graphics.line(112,this.y+130,222,this.y+130);
	love.graphics.printf("Bandit (slowly dying): No.. this.. can't be",112,this.y+140,this.limit,"left");
	love.graphics.line(112,this.y+155,222,this.y+155);
	love.graphics.printf("Sheriff (blowing away gunsmoke): Crime can go suck it!",112,this.y+160,this.limit,"left");
}

Script.show = function () {
	if (!this.showing) {
		this.tween.stop();
		this.showing = true;
		this.tween = Tween.to(this,1,{y:22}).easing("out","quad");
	}
	else {
		if (this.tween) { this.tween.stop() } ;
		this.tween = Tween.to(this,1,{y:180}).easing("out","quad");
		this.showing = false;
	}
}