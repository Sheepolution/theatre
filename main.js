love.load = function () {
	love.graphics.setDefaultFilter("nearest");
	game = Game.new();
}

love.update = function (t) {
	love.mouse.setCursor("default");
	dt = t;
	mouseX = love.mouse.getX()/3;
	mouseY = love.mouse.getY()/3;
	if (Input.keyPressed("r")) {
		love.load();
	}
	Debug.update();
	game.update();
	Tween.update();
	Input.update();
}

love.draw = function () {
	love.graphics.scale(3);
	game.draw();
	Debug.draw();

}

love.config = function (t) {
	//Set the width/height of the canvas
	t.width = 960;
	t.height = 634;
}

//If a key is pressed
love.keypressed = function (key) {
	Input.keyspressed.push(key);
}

//If a mousebutton is pressed
love.mousepressed = function (x,y,button) {
	// mouseX = x*3;
	// mouseY = y*3;
	Input.buttonspressed.push(button);
}

//If a key is pressed
love.keyreleased = function (key) {
	Input.keysreleased.push(key);
}

//If a mousebutton is pressed
love.mousereleased = function (x,y,button) {
	Input.buttonsreleased.push(button);
}

love.graphics.preload(
	"images/stage_back.png",
	"images/stage_front.png",
	"images/cowboy.png",
	"images/bandit.png",
	"images/romeo.png",
	"images/juliet.png",
	"images/romeobackground.png",
	"images/romeoforeground.png",
	"images/tree.png",
	"images/lilman.png",
	"images/man.png",
	"images/script.png",
	"images/good.png",
	"images/bad.png",
	"images/curtains.png",
	"images/flower.png",
	"images/tomato.png",
	"images/review.png",
	"images/logo.png",
	"tut/script.png",
	"tut/actors.png",
	"tut/timer.png",
	"tut/text.png",
	"images/sheepolution.png"
)

love.audio.preload (
	"audio/cowboy.wav",
	"audio/godot.wav",
	"audio/review.wav",
	"audio/romeo.wav"
)

//Initialize love
love.run();