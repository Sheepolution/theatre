Romeo = Play.extend("Romeo");

Romeo.init = function () {
	this.audio = love.audio.newSource("audio/romeo.wav");
	this.audio.setLooping(true);

	this.actors = {
		romeo : Actor.new(80,120),
		juliet : Actor.new(174,28)
	}
	this.assetsBackground = Group.new();
	this.assetsBackground.add(
		Asset.new(120,0,"images/romeobackground.png")
		)

	this.assetsForeground = Group.new();
	this.assetsForeground.add(
		Asset.new(165,46,"images/romeoforeground.png")
	)

	this.actors.romeo.setImage("images/romeo.png",24,41);
	this.actors.romeo.addAnimation("shoulders",2,2);
	this.actors.romeo.addAnimation("down",1,1);
	this.actors.romeo.choseText = false;
	this.actors.romeo.choseState = false;
	this.actors.romeo.anim = "idle";
	this.actors.romeo.states = [
	"*Shrugging shoulders*",
	"\"Eh... I'm not gonna do that. I'm afraid of heights\"",
	"\"Uhm.. behind you?\"",
	"\"Not really.. see ya!\"",
	"\"Yeah.. I was in the neighborhood\""

	];

	this.actors.romeo.stateEffects = [];
	this.actors.romeo.stateEffects[0] = "shoulders";

	// print(this.actors.cowboy.anim);


	// "This is the longest sentence I've ever made!",

	// this.actors.bandit = Actor.new(210,120);
	this.actors.juliet.setImage("images/juliet.png",22,41);
	this.actors.juliet.addAnimation("left",2,2,5);
	this.actors.juliet.states = [
	"*Look left*",
	"\"Romeo! You came to see me, my love!\"",
	"\"Oh. Well, for sure our love is stronger than your fear, is it not?\"",
	"\"Romeo oh Romeo, where art thou, my romeo?\"",
	"\"Oh Romeo, climb these vines so we can be together once more!\""];

	this.actors.juliet.stateEffects = [];
	this.actors.juliet.stateEffects[0] = "left";

	this.actions = [
		{
			actors : {
				juliet : {
					text : "\"Romeo oh Romeo, where art thou, my romeo?\"",
					state : "idle"
				},
				romeo : {
					text : null,
					state : "idle"
				}
			},
			time : 6
		},
		{
			actors : {
				romeo : {
					text : "\"Uhm.. behind you?\"",
					state : "idle"
				},
				juliet : {
					text : null,
					state : "idle"
				}
			},
			time : 6
		},
		{
			actors : {
				juliet : {
					text : "\"Romeo! You came to see me, my love!\"",
					state : "left"
				},
				romeo : {
					text : null,
					state : "idle"
				}
				
			},
			time : 7
		},
		{
			actors : {
				juliet : {
					text : null,
					state : "left"
				},
				romeo : {
					text : "\"Yeah.. I was in the neighborhood\"",
					state : "idle"
				}
			},
			time : 5
		},
		{
			actors : {
				juliet : {
					state : "left",
					text : "\"Oh Romeo, climb these vines so we can be together once more!\""
				},
				romeo : {
					text : null,
					state : "idle"
				}
			},
			time : 5
		},
		{
			actors : {
				romeo : {
					state : "idle",
					text : "\"Eh... I'm not gonna do that. I'm afraid of heights\""
				},
				juliet : {
					text : null,
					state : "left"
				}
			},

			time : 5
		},
		{
			actors : {
				romeo : {
					state : "idle",
					text : null
				},
				juliet : {
					text : "\"Oh. Well, for sure our love is stronger than your fear, is it not?\"",
					state : "left"
				}
			},
			time : 5
		},
		{
			actors : {
				romeo : {
					state : "shoulders",
					text : "\"Not really.. see ya!\""
				},
				juliet : {
					text : null,
					state : "left"
				}
			},
			time : 7
		}
	]


	Romeo.super.init(this);


	this.script.draw = function () {
		love.graphics.draw(this.img,this.x,this.y);
		love.graphics.setFont(Game.fonts.title);
		love.graphics.setColor(80,80,80);
		love.graphics.print("SCRIPT",this.x + 110,this.y + 16);
		love.graphics.setFont(Game.fonts.script);
		love.graphics.printf("Juliet: Romeo oh Romeo, where art thou, my romeo?",112,this.y + 40,this.limit,"left");
		love.graphics.line(112,this.y+53,222,this.y+53);
		love.graphics.printf("Romeo: Uhm.. behind you?",112,this.y+57,this.limit,"left");
		love.graphics.line(112,this.y+64,222,this.y+64);
		love.graphics.printf("Juliet (turning around): Romeo! You came to see me, my love!",112,this.y+67,this.limit,"left");
		love.graphics.line(112,this.y+80,222,this.y+80);
		love.graphics.printf("Romeo: Yeah.. I was in the neighborhood",112,this.y+83,this.limit,"left");
		love.graphics.line(112,this.y+90,222,this.y+90);
		love.graphics.printf("Juliet: Oh Romeo, climb these vines so we can be together once more!",112,this.y+93,this.limit,"left");
		love.graphics.line(112,this.y+107,222,this.y+107);
		love.graphics.printf("Romeo: Eh... I'm not gonna do that. I'm afraid of heights",112,this.y+110,this.limit,"left");
		love.graphics.line(112,this.y+125,222,this.y+125);
		love.graphics.printf("Juliet: Oh. Well, for sure our love is stronger than your fear, is it not?",112,this.y+128,this.limit,"left");
		love.graphics.line(112,this.y+142,222,this.y+142);
		love.graphics.printf("Romeo (shrugging shoulders): Not really.. see ya!",112,this.y+145,this.limit,"left");
	}
}
